import { promises as fs } from 'fs';

let allCitys = [];
let allStates = [];
let citysByStates = [];
let quantitativoCidadesPorEstado = [];
const urlInfra = `./infra/data/`;
const urlCidades = `${urlInfra}/cidades`;
const citysFile = `Cidades.json`;
const statesFile = `Estados.json`;

init();

async function init() {
  await loadData();
  await fillCitysByStates();
  await createAndFillFiles();
  await carregarQuantitativoCidades();
  await imprimirMaioresEstados();
  await imprimirMenoresEstados();
  // await imprimirCidadeMaiorNomePorEstado();
  //await imprimirCidadeMenorNomePorEstado();
  //await imprimirMaiorNomeCidade();
  //await imprimirMenorNomeCidade();
}

async function loadData() {
  try {
    allCitys = JSON.parse(await fs.readFile(`${urlInfra}${citysFile}`));
    allStates = JSON.parse(await fs.readFile(`${urlInfra}${statesFile}`));
  } catch (error) {
    console.log(error);
  }
}

async function fillCitysByStates() {
  citysByStates = allCitys.map((cidade) => {
    const estadoEncontrado = allStates.find(
      (estado) => estado.ID === cidade.Estado
    );
    const { Sigla: sigla, Nome: nomeEstado } = estadoEncontrado;
    return { sigla, nomeEstado, ...cidade };
  });
}

async function createAndFillFiles() {
  try {
    allStates.forEach((state) => {
      const cidadesFiltradasPorEstado = citysByStates.filter((i) => {
        return i.Estado === state.ID;
      });
      fs.writeFile(
        `${urlCidades}/${state.Sigla}.json`,
        JSON.stringify(cidadesFiltradasPorEstado, null, 4)
      );
    });
  } catch (error) {
    console.log(error);
  }
}

async function carregarQuantitativoCidades() {
  try {
    console.log('Carregando quantitativo');
    quantitativoCidadesPorEstado = allStates.map((estado) => {
      const quantidade = citysByStates.filter((i) => i.Estado === estado.ID)
        .length;
      return { ...estado, quantidade };
    });
  } catch (error) {
    console.log(error);
  }
}

async function imprimirMaioresEstados() {
  quantitativoCidadesPorEstado.sort((a, b) => {
    return b.quantidade - a.quantidade;
  });
  console.log('============== MAIORES ESTADOS ================');
  console.log(quantitativoCidadesPorEstado.slice(0, 5));
}

async function imprimirMenoresEstados() {
  quantitativoCidadesPorEstado.sort((a, b) => {
    return a.quantidade - b.quantidade;
  });
  console.log('============== MENORES ESTADOS ================');
  console.log(quantitativoCidadesPorEstado.slice(0, 5));
}

async function imprimirCidadeMaiorNomePorEstado() {
  console.log(
    '========== CIDADE MAIORES NOMES CIDADE POR ESTADO ============='
  );
  const maioresNomesPorEstado = allStates.map((estado) => {
    const cidades = citysByStates.filter((cidade) => {
      return cidade.Estado === estado.ID;
    });
    cidades.sort((a, b) => {
      return b.Nome.length - a.Nome.length || a.Nome.localeCompare(b.Nome);
    });
    const maiorCidade = cidades.slice(0, 1);
    return maiorCidade;
  });
  console.log(maioresNomesPorEstado);
}

async function imprimirCidadeMenorNomePorEstado() {
  console.log('========== CIDADE MENOR NOMES CIDADE POR ESTADO =============');
  const menoresNomesPorEstado = allStates.map((estado) => {
    const cidades = citysByStates.filter((cidade) => {
      return cidade.Estado === estado.ID;
    });
    cidades.sort((a, b) => {
      return a.Nome.length - b.Nome.length || b.Nome.localeCompare(a.Nome);
    });
    const menorCidade = cidades.slice(0, 1);
    return menorCidade;
  });
  console.log(menoresNomesPorEstado);
}

async function imprimirMaiorNomeCidade() {
  console.log('========== Maior Nome de Cidade =============');
  console.log(
    citysByStates
      .sort((a, b) => {
        return b.Nome.length - a.Nome.length || b.Nome.localeCompare(a.Nome);
      })
      .slice(0, 1)
  );
}

async function imprimirMenorNomeCidade() {
  console.log('========== Menor Nome de Cidade =============');
  console.log(
    citysByStates
      .sort((a, b) => {
        return a.Nome.length - b.Nome.length || a.Nome.localeCompare(b.Nome);
      })
      .slice(0, 1)
  );
}
